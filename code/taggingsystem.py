import argparse
import operator
from collections import defaultdict
import utility
import extract

# Some config settings
PARSER = argparse.ArgumentParser(
    description="Tagging System Commad Line Interface.",
    usage="python3.5 taggingsystem.py -file [Input 84cc file] -geofile [Input geonames txt file] -top [top geonameid] -segment [The segment method of system]")

PARSER.add_argument("-file", "--Input84ccFile", type=str, required=True,
                    help="Input 84cc file")
PARSER.add_argument(
    "-geofile",
    "--InputGeonames",
    type=str,
    required=True,
    help="Input geonames txt. If there is more than one geonames.txt, please use ; as separate. [ex. TW.txt or TW.txt;CN.txt ] ")
PARSER.add_argument("-out", "--OutputResultName", type=str, required=True,
                    help="Output result file name")
PARSER.add_argument("-top", "--TopID", type=int, required=False, default=5,
                    help="Pick top K geonames id")
PARSER.add_argument(
    "-segment",
    "--SegmentMethod",
    type=str,
    required=False,
    default='j23456',
    help="The segment method of system, Default: j23456, means jieba + n-gram (n = 2, 3, 4, 5, 6); 'j' means jieba only")

PARSER.set_defaults(argument_default=False)
CONFIG = PARSER.parse_args()

# Some fields does not belong to place description.
ignor = ['TWD97座標 X', 'TWD97座標 Y', '[海拔高度 (公尺)]', 'coordinates定位', 'time period時代', '北緯', '國別代碼', '地質年代', '座標（GPS)', '拍攝日期', '採集方法',
         '採集經度', '採集緯度', '採集深度', '最低海拔', '最大海拔', '最小海拔', '最高海拔', '朝代', '標本館號', '海拔', '深度', '滅絕地質年代', '經度',  # 生物
         '緯度', '開始地質年代', '東經', '面積', '地質年代', ' 深度（公尺）', '經緯度', '原始採集資訊', ' 深度（公尺）', '經緯度', '經緯度(度分秒)',
         '社名', '經度', '緯度', '面積', '所屬族群', '現行政區（代碼）',  # 人類學
         '地質年代', '產地', '開始地質年代', '滅絕地質年代', '地質分佈', '用途', '地層', '地質分區',  # 地質
         'Temporal',  # 影音
         'coordinates定位', 'time period時代', '座標（GPS)',  # 建築
         '拍攝日期', '朝代',  # 檔案
         '遺址之文化期', '東經', '北緯', '遺址之文化時期', '地形區'  # 考古
         ]


def GetCoverage(Object, colnames, data):

""" Get the content of Coverage and Construct the hierarchy of coverage
    Return Object: A dictionary object, key: oid, value: a list of tuple
    (field/dwc/None, coverage content)
"""
    if 'Coverage' not in colnames:
        return {}

    coverageindex = colnames.index('Coverage')
    coverageData = defaultdict(list)
    for oid in Object:
        contents = []
        if 'Coverage::field' in Object[oid]:
            for j in Object[oid]['Coverage::field']:
                if oid in [
                    '1099850',
                        '2146001']:  # Special Process for oid 1099850, 2146001 (生物)
                    contents.append("%s:%s" % (
                        j[1], data[int(j[0])][coverageindex].replace(':', ';')))
                # Special Process for oid 3340557, 3340562 (生物)
                elif oid in ["3340557", "3340562", "5982157"]:
                    contents.append("%s:%s" % (
                        j[1], data[int(j[0])][coverageindex].replace(':', ' , ')))
                else:
                    contents.append("%s:%s" %
                                    (j[1], data[int(j[0])][coverageindex]))
        if 'Coverage::dwc' in Object[oid]:
            for j in Object[oid]['Coverage::dwc']:
                contents.append("%s:%s" %
                                (j[1], data[int(j[0])][coverageindex]))
        if 'Coverage::field' not in Object[oid] and 'Coverage' in Object[oid]:
            for j in Object[oid]['Coverage']:
                if oid == "1926440":  # Special Process for oid 1926440 (生物)
                    token = j[1].split(':')
                    contents.append(
                        "%s:%s-%s" %
                        (token[0], token[1], token[2]))
                else:
                    contents.append("%s" % (j[1]))

        segmentcontent = []
        for content in contents:
            if ';' in content:
                if '遺址之經緯度' in content:  # Special process for 考古
                    segmentcontent += [
                        '遺址之經緯度:%s' %
                        (token) if '遺址之經緯度' not in token else token for token in content.split(';')]
                else:
                    segmentcontent += content.split(';')
            elif '；' in content:
                segmentcontent += content.split(';')
            else:
                segmentcontent.append(content)
                if '臺灣大學人類學系典藏文物數位化計畫-人類學.csv' in CONFIG.Input84ccFile:  # Special process in 人類學
                    segmentcontent += extract.Removebullet(content)
                elif "南京教區契約文書數位典藏計畫.csv" in CONFIG.Input84ccFile:  # Special process in 檔案
                    segmentcontent += content[3:].split('。')

        for content in segmentcontent:
            content = utility.clearspace(content)
            if content == '':
                continue

            if '：' in content:
                token = content.split('：')
            elif ':' in content:
                token = content.split(':')
            elif '北緯' in content and '東經' in content:
                coverageData[oid].append(('經緯度(度分秒)', content))
            else:
                coverageData[oid].append(('', content))
                continue

            if len(token) == 2:
                if token[1] == ' ' or token[1] == '':  # Only coverage field
                    coverageData[oid].append(('', token[0]))
                elif token[0] == ' ' or token[0] == '':
                    coverageData[oid].append(
                        ('', token[1]))  # Only coverage value
                else:
                    coverageData[oid].append((token[0], token[1]))
            elif '緯度' in token[0] or '經度' in token[0]:  # Special process in 人類學
                coverageData[oid].append((token[0], ":".join(token[1:])))
            elif "臺灣大學地質科學典藏數位化計畫.csv" in CONFIG.Input84ccFile:  # Special process in 地質
                coverageData[oid].append(('', content.replace(':', ' ')))
            elif "林業試驗所植物標本館典藏數位化計畫.csv" in CONFIG.Input84ccFile:  # Special process in 生物
                coverageData[oid].append((token[0], " , ".join(token[1:])))
            elif "台灣昆蟲與螨類資源數位館之建立.csv" in CONFIG.Input84ccFile:  # Special process in 生物
                coverageData[oid].append(
                    ("%s(%s)" % (token[0], token[1]), token[2]))
            else:
                coverageData[oid].append(('', content))

    return coverageData


def ReadGeonameTable(geoname):

""" Read the Geoname table """

    GeonameTable, PlaceTable = {}, defaultdict(list)
    with open(geofile) as f:
        for line in f.read().splitlines():
            token = line.split('\t')
            geoid = token[0]
            GeonameTable[geoid] = {}
            GeonameTable[geoid]['geonameid'] = geoid
            GeonameTable[geoid]['name'] = token[1]
            GeonameTable[geoid]['asciiname'] = token[2]
            place = token[3].split(',')
            for p in place:
                PlaceTable[p].append(geoid)
            GeonameTable[geoid]['alternatenames'] = place
            GeonameTable[geoid]['latitude'] = float(token[4])
            GeonameTable[geoid]['longitude'] = float(token[5])
            GeonameTable[geoid]['feature_class'] = token[6]
            GeonameTable[geoid]['feature_code'] = token[7]
            GeonameTable[geoid]['country code'] = token[8]
            GeonameTable[geoid]['cc2'] = token[9]
            GeonameTable[geoid]['admin1_code'] = token[10]
            GeonameTable[geoid]['admin2_code'] = token[11]
            GeonameTable[geoid]['admin3_code'] = token[12]
            GeonameTable[geoid]['admin4_code'] = token[13]
            GeonameTable[geoid]['population'] = token[14]
            GeonameTable[geoid]['elevation'] = token[15]
            GeonameTable[geoid]['dem'] = token[16]
            GeonameTable[geoid]['timezone'] = token[17]
            GeonameTable[geoid]['modification_date'] = token[18]

    return GeonameTable, PlaceTable


def DumpResult(
        coverageData,
        coverageMappingTerms,
        coverageMappingID,
        coverageLongLat,
        geofiles):

""" Dump the result """

    StoreDat = ["OID,Coverage::field,Coverage,Detect_Long,Detect_Lat,Extract_Terms,Recommend_Top1,%s" % (
        ','.join(["Recommend_%s" % (geoname) for geoname in geofiles]))]
    for oid in coverageData:
        storestring, fields, contents = [oid], [], []
        for (field, content) in coverageData[oid]:
            fields.append(field)
            contents.append(content)
        storestring.append(";".join(fields))
        storestring.append(";".join(contents))
        if oid in coverageLongLat:
            Long, Lat = coverageLongLat[oid]
            storestring.append(str(Long))
            storestring.append(str(Lat))
        else:
            storestring.append('')
            storestring.append('')
        terms = []
        for geoname in geofiles:
            for (gid, term) in coverageMappingTerms[geoname][oid]:
                terms.append(term)
        storestring.append(';'.join(terms))
        if coverageMappingID[oid]['recommend'] is not None:
            storestring.append(coverageMappingID[oid]['recommend'])
            for geoname in geofiles:
                places = [pair[0]
                          for pair in coverageMappingID[oid][geoname][:CONFIG.TopID]]
                storestring.append(';'.join(places))
        else:
            storestring.append('')
            storestring += [''] * len(geofiles)
        StoreDat.append(",".join(storestring))
    with open(CONFIG.OutputResultName, 'w') as f:
        f.write('\n'.join(StoreDat))


if __name__ == "__main__":

    # Read 84cc file data and process the term
    Object, colnames, data = utility.Read(CONFIG.Input84ccFile)
    coverageData = GetCoverage(Object, colnames, data)
    # Extract the latitude and longitude from coverage data
    coverageLongLat = extract.FindLongLat(coverageData)
    # Segment the coverage data into terms with the CONFIG.SegmentMethod (ex:
    # j34) settings
    coverageSegmentTerm = extract.SegmentTerm(
        coverageData, ignor, CONFIG.SegmentMethod)

    # Read Geoname Table
    coverageMappingID, coverageMappingTerms, score_mappingID = {}, {}, {}
    geofiles = []
    for geofile in CONFIG.InputGeonames.split(';'):
        # Read Geoname DB
        geoname = geofile.split('/')[-1]
        geofiles.append(geoname)
        GeonameTable, PlaceTable = ReadGeonameTable(geoname)
        # Search for match places
        mappingid, mappingterm = extract.Search(
            coverageSegmentTerm, PlaceTable)
        coverageMappingTerms[geoname] = mappingterm
        # Evaluate the score for each places
        score_mappingID[geoname] = extract.evaluateScore(
            mappingid, mappingterm, coverageLongLat, GeonameTable, geoname)

    # Find the highest score place among all geoname tables
    coverageMappingID = extract.ReRank(coverageData, score_mappingID)
    # Dump Result
    DumpResult(
        coverageData,
        coverageMappingTerms,
        coverageMappingID,
        coverageLongLat,
        geofiles)
