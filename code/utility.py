import csv
import re
import string
import unicodedata
import operator


def Read(FileName, csvname=''):

""" Read Data in CSV FILE
The function will return 3 variables.
1. csv object, which is stored in a dictionary structure.
    Object -> 'OID': { 'OID': oid,
                    'DAURI': [(lin_number, content)],
                    'Title': [(lin_number, content), ..., (lin_number, content)],
                    'Title::field': [(lin_number, content), ..., (lin_number, content)],
                    ...: ...,
                    ...: ...,
                    }
2. column, csv header, stored in a list
3. data, raw data of csv file, stored in a 2D list.
"""
    Obj = {}
    data = []
    with open(FileName, newline='') as f:
        dat = csv.reader(f, delimiter=',', quotechar='"')
        for i in dat:
            data.append(i)

    colnames = data[0]
    colnames[0] = "OID"

    objid = 'ID'
    for linenum, line in enumerate(data[1:], 1):
        for e, x in enumerate(line):
            if x != '':
                x = x.replace(",", "，")
                if e == 0:
                    if x not in Obj:
                        objid = x
                        Obj[objid] = {}
                        Obj[objid][colnames[e]] = (str(linenum), x)
                        Obj[objid]['CSVDAT'] = csvname
                else:
                    if colnames[e] not in Obj[objid]:
                        Obj[objid][colnames[e]] = []
                    Obj[objid][colnames[e]].append((str(linenum), x))

    return Obj, colnames, data


def CSVWRITETOFILE(X, filename):

""" Write data into csv file """
    with open(filename, "w", newline='') as f:
        f.write('\ufeff')
        writer = csv.writer(
            f,
            delimiter=',',
            quotechar='"',
            quoting=csv.QUOTE_ALL)
        writer.writerows(X)


def is_number(s):

""" Test a textstring is a number textstring or not """
    try:
        float(s)
        return True
    except ValueError:
        pass

    try:
        unicodedata.numeric(s)
        return True
    except (TypeError, ValueError):
        pass

    return False


def String_type(term):

""" Return textstring types
    -1: Nothing textstring
    0: Mixture textstring, contain number, Chinese, English punctuation character.
    1: English textstring, with punctuation or number.
    2: Chinese textstring, with punctuation or number.
    3: English textstring
    4: Chinese textstring
"""

    E = 0
    C = 0
    P = 0
    N = 0
    for w in term:
        if unicodedata.category(w) == 'Lu' or unicodedata.category(w) == 'Ll':
            E += 1  # English character
        elif unicodedata.category(w) == 'Lo':
            C += 1  # Chinese character
        elif unicodedata.category(w).startswith('P'):
            P += 1  # Puncaution
        elif unicodedata.category(w) == 'Nd':
            N += 1  # Number

    textstring_type = -1
    if E != 0 and C == 0 and N == 0:  # English textstring
        textstring_type = 1
    elif E == 0 and C != 0 and N == 0:  # Chinese textstring
        textstring_type = 2
    elif E != 0 and C == 0:
        textstring_type = 3
    elif E == 0 and C != 0:
        textstring_type = 4
    elif E != 0 or C != 0 or P != 0 or N != 0:
        textstring_type = 0  # Mixture textstring

    return textstring_type


def segment_en_zh(textstring):
    ### Segmenstring  ###
    # For example, Happy Birthday生日快樂 Return ['Happy Birthday', '生日快樂']
    # Testing English text我爱蟒蛇 -> ['Testing English text', '我爱蟒蛇']
    # 我爱Python -> ['我爱', 'Python']

    newtextstring = []
    textstring = clearspace(textstring)
    types = {
        string.ascii_letters: 'E',
        string.digits: 'D',
        string.punctuation: 'P',
        string.whitespace: 'S'}
    start, startcount = '', 0
    for e, w in enumerate(textstring):
        now = 'C'
        for key in types:
            if w in key:
                now = types[key]
        if start == '':  # Decode Start Typr
            start = now
            continue
        if start in ['E', 'D', 'P', 'S'] and now == 'C':
            newtextstring.append(textstring[startcount:e])
            start, startcount = now, e
        elif start == 'C' and now in ['E', 'D', 'P', 'S']:
            newtextstring.append(textstring[startcount:e])
            start, startcount = now, e

    newtextstring.append(textstring[startcount:])

    return newtextstring


def clearspace(textstring):

""" Clear space in a textstring
    For example, '  國立臺灣大學  '
    Return '國立臺灣大學'
"""
    newtextstring = ''
    for w in textstring:
        if not unicodedata.category(w).startswith('C'):
            newtextstring += w

    textstring = newtextstring
    while textstring.startswith(' '):
        textstring = textstring[1:]
    while textstring.endswith(' '):
        textstring = textstring[:-1]

    return textstring


def Split_to_String(textstring):

""" Segment a textstring into sentences.
    '地點: 高雄（高雄縣）、台南市'
    Return ['高雄', '高雄縣', '台南市']
"""

    textstring = textstring.replace('(', '、')
    textstring = textstring.replace(')', '、')
    textstring = textstring.replace('（', '、')
    textstring = textstring.replace('）', '、')
    textstring = textstring.replace('-', '、')
    textstring = textstring.replace('\'', '、')
    textstring = textstring.replace('．', '、')
    textstring = textstring.replace('－', '、')
    textstring = textstring.replace('，', '、')
    textstring = textstring.replace(',', '、')
    textstring = textstring.replace('。', "、")

    temptoken = []
    for token in segment_en_zh(textstring):
        if String_type(token) == 2:
            token = token.replace('.', '、')
        temptoken += token.split('、')

    return temptoken
