# Tool for Geonames Mapping

author: Hsin-Ping Chen

# Introduction

We provide a tool for mapping place names from the `Coverage` (dc:coverage) to the place IDs on geonames.org.

* Input:
  1. csv files converted from xml files of Union Catalog of Digital Archives Taiwan
  2. placenames from [geonames.org] (http://www.geonames.org)

* Options:
  1. `-file`: the input 84cc csv file.
  2. `-geofile`: the input geonames txt. If there is more than one geonames.txt, please use `;` as separate. [ex. "TW.txt" or "TW.txt;CN.txt" ]
  3. `-out`: the output result filename.
  4. `-top` (optional): the top K mapped geonames id.
  5. `-segment` (optional): the segment method, default is "j23456", means jieba + n-gram (n = 2, 3, 4, 5, 6); 'j' means jieba only"

* Output:
  1. the column `CoverageContent` is the original content from input csv files (which is concatenated of `Coverage` and `Coverage::field`)
  2. the match results in csv files with the following fields and values:

| Field | Value |
| ----- | ----- |
| OID | (from input csv) |
| Coverage::field | The original coverage field value of OID, separated by semicolons |
| Coverage | The original coverage field value of OID, separated by semicolons |
| Detect_Long | The latitude extracted from the field `Coverage` |
| Detect_Lat | The latitude extracted from the field `Coverage` |
| Extract_Terms | The extract terms from field `Coverage` |
| Recommend_Top1 | Choose the highest score from the mapping geonameid |
| Recommend_TW.txt | The matched geonames id from TW.txt, separated by semicolons |
| Recommend_CN.txt | The matched geonames id from CN.txt, separated by semicolons |
| Recommend_JP.txt | The matched geonames id from JP.txt, separated by semicolons |


# Usage

1. Check that `place.dict` and `Transform.txt` are at folder `dicts/`
2. Revise the value of `datdir`, `geonames` and other arguments in of `script/run_tagging.sh`
3. Run `script/run_tagging.sh`

