datdir="/service/84cc/revised"
geonames="geonames/TW.txt;geonames/CN.txt;geonames/JP.txt"
top="10"
segment="j34"
PYTHON="python3.5"

set -x
for Group in 內容主題-人類學 內容主題-生物 內容主題-考古 內容主題-器物 內容主題-學術社會應用推廣 內容主題-影音 內容主題-新聞 內容主題-檔案 內容主題-善本古籍 內容主題-地質 內容主題-拓片 內容主題-建築
do
    mkdir -p ./result/$Group/
    ls $datdir/$Group | while read -r file
    do
        $PYTHON ./code/taggingsystem.py -geofile "$geonames" -file "$datdir/$Group/$file" -out "./result/$Group/$file" -top "$top" -segment "$segment"
    done
done

