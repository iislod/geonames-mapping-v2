import csv
import re
import string
import unicodedata
import operator
import twd97
import jieba
from operator import itemgetter
from collections import defaultdict
import utility
jieba.load_userdict("dicts/place.dict")


def Removebullet(content):
    # Remove the bullet in coverage description
    # EX. 1.舊地區：壯二結
    # EX. 2.土名：茄苳林，現行政區（代碼）：1000206-002

    for n in range(1, 10):
        term = '%d.' % (n)
        if term in content:
            content = content.replace(term, ';')

    segment_content = []
    tokens = content.replace('，', ';').split(';')
    for token in tokens:
        if token == '' or token == ' ':
            continue
        segment_content.append(utility.clearspace(token))

    return segment_content


def FindLongLat(coverageData):
    # Extract the latitude and longitude from coverage data

    LongStr = ['經度', '東經', '採集經度', 'TWD97座標 Y']
    LatStr = ['緯度', '北緯', '採集緯度', 'TWD97座標 X']
    count = 0
    coverageLongLat = {}

    for oid in coverageData:
        Lat = ''
        Long = ''
        for coverage_e, (field, content) in enumerate(coverageData[oid]):
            if utility.is_number(content) and field in LatStr:
                Lat = float(content)

            if utility.is_number(content) and field in LongStr:
                Long = float(content)

            if field in LongStr and 'X:' in content and utility.is_number(
                    content.split(':')[-1]):
                Long = float(content.split(':')[-1])

            if field in LatStr and 'Y:' in content and utility.is_number(
                    content.split(':')[-1]):
                Lat = float(content.split(':')[-1])

            if 'TWD97座標' in field and Long != '' and Lat != '':
                Lat, Long = twd97.towgs84(Lat, Long)

            if field == 'coordinates定位':
                e, n = content.split(';')[0].split(' ')
                Lat = float(n[1:])
                Long = float(e[1:])

            if field == u'coordinates\u5b9a\u4f4d':
                token = content.split(' ')
                for t in token:
                    if 'E' in t:
                        Long = float(t[1:])
                    if 'N' in t:
                        Lat = float(t[1:-1])

            if field == '座標（GPS)':
                e, n = content.split(',')
                Lat = GPStransform_1(n)
                Long = GPStransform_1(e)

            if '遺址之經緯度' in field:
                if ';' in content:
                    e, n = content.split(':')[1].split(';')
                    Lat = GPStransform_2(n)
                    Long = GPStransform_2(e)
                elif '\'\'\'' in content:
                    e, n = content.split("'''")
                    Lat = GPStransform_2(n)
                    Long = GPStransform_2(e)
                elif '東經' in content:
                    Long = GPStransform_2(utility.clearspace(content))
                elif '北緯' in content:
                    Lat = GPStransform_2(utility.clearspace(content))

            if '北緯' in content and '東經' in content and '遺址之經緯度' not in field:
                # if field == '':
                #    coverageData[oid][coverage_e][0] = '經緯度'
                #    print ("ADD", oid, coverageData[oid])
                n, e = content.split(' ')
                temp_n = n.split('北緯')[1].replace('\'', '')
                temp_e = e.split('東經')[1].replace('\'', '')
                if 'unknow' in temp_n or 'unknow' in temp_e:
                    continue
                Lat = GPStransform_3(temp_n)
                Long = GPStransform_3(temp_e)

            if '經緯度(度分秒)' in field:
                n, e = content.split(' ')
                Long = GPStransform_3(n[2:])
                Lat = GPStransform_3(e[2:])

            # if '北緯：' in content:
            #    temp_n = content.split('：')[-1]
            #    Lat = float(temp_n)
            # if '東經：' in content:
            #    temp_e = content.split('：')[-1]
            #    Long = float(temp_e)

            if Long != '' and Lat != '':
                coverageLongLat[oid] = (Long, Lat)

    return coverageLongLat


def GPStransform_1(pos):

""" The GPS transform rule 1 """

    gps = 0.0
    d, ms = pos.split(u'\ufffd')
    gps += float(d)
    m, s = ms.split('\'')
    gps += (float(m) + float(s) / 60.0) / 60.0

    return round(gps, 6)


def GPStransform_2(pos):

"""
The GPS transform rule 2
EX: 東經120度48'23'' or 北緯21度56'15''
"""
    gps = 0.0
    while pos[0] == ' ':  # remove space
        pos = pos[1:]
    d, ms = pos[2:].split('度')
    if utility.is_number(d):
        gps += float(d)
    token = ms.split('\'')
    m = token[0]
    s = token[1]
    gps += (float(m) + float(s) / 60.0) / 60.0

    return round(gps, 6)


def GPStransform_3(gpsstr):

""" The GPS transform rule 3 """
    gpsstr = utility.clearspace(gpsstr)
    if gpsstr[-1] == '/' or gpsstr[-1] == '\'':
        gpsstr = gpsstr[:-1]

    gps = 0.0
    if len(gpsstr.split('/')) == 1:
        d = float(gpsstr)
        m = 0.0
        s = 0.0
    elif len(gpsstr.split('/')) == 2:
        d, m = gpsstr.split('/')
        s = 0.0
        if '~' in m:
            m1, m2 = m.split('~')
            m = (float(m1) + float(m2)) / 2.0
        elif '-' in m:
            m1, m2 = m.split('-')
            m = (float(m1) + float(m2)) / 2.0
        elif m == '':
            m = 0.0
    elif len(gpsstr.split('/')) == 3:
        d, m, s = gpsstr.split('/')
        if s == '':
            s = 0.0
        if '~' in m:
            m1, m2 = m.split('~')
            m = (float(m1) + float(m2)) / 2.0
        elif '-' in m:
            m1, m2 = m.split('-')
            m = (float(m1) + float(m2)) / 2.0
        elif m == '':
            m = 0.0
    elif len(gpsstr.split('/')) == 4:
        d, m, s, t = gpsstr.split('/')
        if t == '':
            if s == '':
                s = 0.0
            if '~' in m:
                m1, m2 = m.split('~')
                m = (float(m1) + float(m2)) / 2.0
            elif '-' in m:
                m1, m2 = m.split('-')
                m = (float(m1) + float(m2)) / 2.0
            elif m == '':
                m = 0.0

    gps += float(d) + (float(m) + float(s) / 60.0) / 60.0

    return round(gps, 6)


def GPStransform_5(gpsstr):

""" The GPS transform rule 5
## EX 1: "北緯22/00' 東經120/44"
## EX 2: "北緯25/02/02' 東經121/30"
## EX 3: "北緯23/00/' 東經120/42/"
"""

    gpsstr = utility.clearspace(gpsstr)
    if gpsstr[-1] == '/' or gpsstr[-1] == '\'':
        gpsstr = gpsstr[:-1]

    gps, d, m, s = 0.0, 0.0, 0.0, 0.0
    dms = gpsstr.split('/')
    if len(dms) > 0 and dms[0] != '':
        d = float(dms[0])
    if len(dms) > 1 and dms[1] != '':
        m = float(dms[1])
    if len(dms) > 2 and dms[2] != '':
        s = float(dms[2])

    gps = float(d) + (float(m) + float(s) / 60.0) / 60.0
    return round(gps, 6)


def SegmentTerm(coverageData, ignor, SegmentMethod):

""" Segment the coverage data into terms with the CONFIG.SegmentMethod (ex: j34) settings """

    Transform = {}
    with open("dicts/Transform.txt") as f:
        for line in f.read().splitlines():
            token = line.split(' ')
            name = token[0]
            replacename = " ".join(token[1:])
            Transform[name] = replacename

    coverageSegmentTerm = {}
    for oid in coverageData:
        SegmentTerm = []
        for (field, content) in coverageData[oid]:
            if field in ignor or '??' in content:
                continue

            # Start to Segment content value
            SegmentTerm.append(content)  # Keep original content value
            sentences = utility.Split_to_String(content)
            for sent in sentences:
                SegmentTerm.append(sent)  # Keep sentence pattern
                terms = re.findall(
                    r"\w+|[^\s\w]", sent, re.UNICODE)  # split to punctuation
                for term in terms:
                    if len(term) > 1:
                        for n in list(SegmentMethod):  # N-gram
                            if n in string.digits:
                                n = int(n)
                                if utility.String_type(
                                        term) == '1':  # English term
                                    ngrams = ngrams(term.split(' '), n)
                                else:  # Guess Chinese term
                                    ngrams = [term[i:i + n]
                                              for i in range(len(list(term)) - n + 1)]
                                for word in ngrams:
                                    SegmentTerm.append(
                                        utility.clearspace(word))
                        if 'j' in SegmentMethod:
                            for word in jieba.cut(term):
                                if word != '' or word != ' ':
                                    SegmentTerm.append(word)

        # Add term's alternative name
        for w in SegmentTerm:
            if w in Transform:
                SegmentTerm.append(Transform[w])
        # Unique the mapping id
        coverageSegmentTerm[oid] = [
            w for w in set(SegmentTerm) if w != ' ' and w != '']

    return coverageSegmentTerm


def Search(coverageSegmentTerm, PlaceTable):

""" Search for match places """

    coverageMappingID, coverageMappingTerms = {}, {}
    for oid in coverageSegmentTerm:
        mappingid = []
        coverageMappingTerms[oid] = defaultdict(int)
        for term in coverageSegmentTerm[oid]:
            if term in PlaceTable:
                mappingid += PlaceTable[term]
                for gid in PlaceTable[term]:
                    coverageMappingTerms[oid][(gid, term)] += 1
        coverageMappingID[oid] = list(set(mappingid))

    return coverageMappingID, coverageMappingTerms


def evaluateScore(
        mappingid,
        mappingterm,
        coverageLongLat,
        GeonameTable,
        geonameDB):

""" Evaluate the place score """
    score = {}
    for oid in mappingid:
        score[oid] = defaultdict(int)
        for (gid, term) in mappingterm[oid]:
            if 'TW.txt' in geonameDB:  # Recommed taiwan place more
                score[oid][gid] += 2
            GeoObject = GeonameTable[gid]
            score[oid][gid] += (mappingterm[oid][(gid, term)]) ** 0.7
            if GeoObject['feature_class'] == 'A':
                score[oid][gid] += 2
            # P: city, village, .. get no score
            elif GeoObject['feature_class'] in ['H', 'L', 'R', 'S', 'T', 'U', 'V']:
                score[oid][gid] += 1
            if oid in coverageLongLat:
                clong, clat = coverageLongLat[oid]
                glong, glat = GeoObject['longitude'], GeoObject['latitude']
                if ((glong - clong) ** 2 + (glat - clat) ** 2) < 0.001:
                    score[oid][gid] += 4
                elif ((glong - clong) ** 2 + (glat - clat) ** 2) > 1.5:
                    score[oid][gid] -= 3
    return score


def ReRank(coverageData, score_mappingID):

""" Find the highest score place among all geoname tables """

    geonameDB = score_mappingID.keys()
    coverageMappingID = {}
    for oid in coverageData:
        coverageMappingID[oid] = {}
        highestID, highest_ = None, 0
        for geoname in geonameDB:
            sorted_ = sorted(
                score_mappingID[geoname][oid].items(),
                key=operator.itemgetter(1),
                reverse=True)
            coverageMappingID[oid][geoname] = sorted_
            if len(sorted_) > 1 and sorted_[0][1] > highest_:
                highestID, highest_ = sorted_[0]
        coverageMappingID[oid]['recommend'] = highestID

    return coverageMappingID
